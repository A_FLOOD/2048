function MatrixModel() {
  BaseModel.call(this);
  this.attributes = {
    size: { width: 4, height: 4 },
    grid: [
      ['', '', '', ''],
      ['', '', '', ''],
      ['', '', '', ''],
      ['', '', '', '']
    ]
  };

  var instance = this;
  MatrixModel = function () {
    return instance;
  };

  this.showRandomCellWithValue();

}

MatrixModel.prototype = Object.create(BaseModel.prototype);
MatrixModel.prototype.constructor = MatrixModel;

MatrixModel.prototype.randomCell = function() {
  return Math.floor(Math.random() * 4);
};

MatrixModel.prototype.randomValue = function() {
  return Math.random() < 0.5 ? 2 : 4;
};

MatrixModel.prototype.flattenArray = function() {
  return this.attributes.grid.reduce(function (a, b) {
    return a.concat(b);
  })
};

MatrixModel.prototype.transformInMatrix = function(flattenArray) {
  var newArr = [], size = flattenArray.length, i;
  for(i = 0; i < size; i += 4) {
    newArr.push(flattenArray.slice(i, i + 4));
  }
  this.attributes.grid = newArr;
};

MatrixModel.prototype.showRandomCellWithValue = function () {
  this.attributes.grid[this.randomCell()][this.randomCell()] = this.randomValue();
  var flattenArray = this.flattenArray(), newArr = [], i, size = flattenArray.length;
  for(i = 0; i < size; i += 1) {
    if(typeof flattenArray[i] !== 'number') newArr.push(i);
  }
  flattenArray[newArr[Math.floor(Math.random() * newArr.length)]] = this.randomValue();
  this.transformInMatrix(flattenArray);
};

MatrixModel.prototype.transformToColumn = function() {
  var columns = [[], [], [], []], i, j, attributes = this.attributes.grid;
  for (i = 0; i < attributes.length; i += 1) {
    for (j = 0; j < attributes[i].length; j += 1) {
      columns[i].push(attributes[j][i]);
    }
  }
  return columns;
};

MatrixModel.prototype.transformFromColumn = function(columns_array) {
  var matrix = [[], [], [], []], i, j;
  for (i = 0; i < columns_array.length; i++) {
    for (j = 0; j < columns_array[i].length; j++) {
      matrix[i].push(columns_array[j][i]);
    }
  }
  return matrix;
};

MatrixModel.prototype.moveLogic = function(data, subArr, key) {
  if(key === 'left' || key === 'up') {
    typeof data === 'number' ? subArr.unshift(data) : subArr.push(data);
  } else if(key === 'right' || key === 'down') {
    typeof data !== 'number' ? subArr.unshift(data) : subArr.push(data);
  }
};

MatrixModel.prototype.move = function(key) {
  var attributes = this.attributes.grid, attributes_size = attributes.length, i, j, arr = [[], [], [], []];
  if(key === 'left') {
    for(i = 0; i < attributes_size; i += 1) {
      for (j = attributes[i].length - 1; j >= 0; j -= 1) {
        this.moveLogic(attributes[i][j], arr[i], key);
      }
    }
    this.attributes.grid = arr;
  } else if(key === 'right') {
    for(i = 0; i < attributes_size; i += 1) {
      for (j = 0; j < attributes[i].length; j += 1) {
        this.moveLogic(attributes[i][j], arr[i], key);
      }
    }
    this.attributes.grid = arr;
  } else if(key === 'down') {
        var column = this.transformToColumn(), size = column.length, k, l;
        for(k = 0; k < size; k += 1) {
          for (l = 0; l < column[k].length; l += 1) {
            this.moveLogic(column[k][l], arr[k], key);
          }
        }
      this.attributes.grid = this.transformFromColumn(arr);
      } else if(key === 'up') {
        var column_new = this.transformToColumn(), column_size = column_new.length, d, e;
        for(d = 0; d < column_size; d += 1) {
          for (e = column_new[d].length - 1; e >= 0; e -= 1) {
            this.moveLogic(column_new[d][e], arr[d], key);
          }
        }
      this.attributes.grid = this.transformFromColumn(arr);
      }
  this.publish('changedData');
};