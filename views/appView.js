function AppView() {
  var matrixView = new MatrixView();

  this.render = function (selector) {
    var element = document.getElementById(selector);
    matrixView.show(element);
  }
}

var appView = new AppView();
appView.render('root');